class ApplicationController < ActionController::Base
  protect_from_forgery
  
  def index
    render :layout => "home_layout"
  end
  
  def landing_page
    render :layout => false
  end
  
  def add_subscriber
    if(params[:email_or_phone])
      subscriber=Subscriber.new
      subscriber.email_or_phone=params[:email_or_phone]
      subscriber.save
    end
    flash[:notice] = "Thank you."
  end
  
end