class VenusController < ApplicationController
  # GET /venus
  # GET /venus.json
  def index
    @venus = Venu.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @venus }
    end
  end

  # GET /venus/1
  # GET /venus/1.json
  def show
    @venu = Venu.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @venu }
    end
  end

  # GET /venus/new
  # GET /venus/new.json
  def new
    @venu = Venu.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @venu }
    end
  end

  # GET /venus/1/edit
  def edit
    @venu = Venu.find(params[:id])
  end

  # POST /venus
  # POST /venus.json
  def create
    @venu = Venu.new(params[:venu])

    respond_to do |format|
      if @venu.save
        format.html { redirect_to @venu, notice: 'Venu was successfully created.' }
        format.json { render json: @venu, status: :created, location: @venu }
      else
        format.html { render action: "new" }
        format.json { render json: @venu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /venus/1
  # PUT /venus/1.json
  def update
    @venu = Venu.find(params[:id])

    respond_to do |format|
      if @venu.update_attributes(params[:venu])
        format.html { redirect_to @venu, notice: 'Venu was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @venu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /venus/1
  # DELETE /venus/1.json
  def destroy
    @venu = Venu.find(params[:id])
    @venu.destroy

    respond_to do |format|
      format.html { redirect_to venus_url }
      format.json { head :ok }
    end
  end
end
