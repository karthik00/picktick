require 'test_helper'

class VenusControllerTest < ActionController::TestCase
  setup do
    @venu = venus(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:venus)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create venu" do
    assert_difference('Venu.count') do
      post :create, venu: @venu.attributes
    end

    assert_redirected_to venu_path(assigns(:venu))
  end

  test "should show venu" do
    get :show, id: @venu.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @venu.to_param
    assert_response :success
  end

  test "should update venu" do
    put :update, id: @venu.to_param, venu: @venu.attributes
    assert_redirected_to venu_path(assigns(:venu))
  end

  test "should destroy venu" do
    assert_difference('Venu.count', -1) do
      delete :destroy, id: @venu.to_param
    end

    assert_redirected_to venus_path
  end
end
