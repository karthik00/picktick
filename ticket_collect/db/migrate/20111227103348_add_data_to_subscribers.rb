class AddDataToSubscribers < ActiveRecord::Migration
  def self.up
    add_column :subscribers, :email_or_phone, :string
  end

  def self.down
    remove_column :subscribers, :email_or_phone
  end
end